# Multi-Process Service (MPS) [![build status](https://gitlab.com/nvidia/mps/badges/master/build.svg)](https://gitlab.com/nvidia/mps/commits/master)
- [`latest` (*Dockerfile*)](https://gitlab.com/NVIDIA/mps/blob/master/Dockerfile)

# Documentation
https://docs.nvidia.com/deploy/pdf/CUDA_Multi_Process_Service_Overview.pdf
